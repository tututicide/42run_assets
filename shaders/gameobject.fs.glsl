#version 410

uniform mat4 view_mat;

in vec2 texture_coordinates;
in vec3 color;
in vec3 position_eye;
in vec3 normal_eye;

//fixed point light
in vec3 light_position_world;// = vec3(0.0, 0.0, 10.0);
vec3 ls = vec3(0.5, 0.5, 0.5);
vec3 ld = vec3(10.0, 10.0, 10.0);
vec3 la = vec3(0.2, 0.2, 0.2);

//surface reflectance
vec3 ks = vec3(1.0, 1.0, 1.0);
vec3 kd = color;
vec3 ka = vec3(1.0, 1.0, 1.0);
float specular_exponent = 20.0;

out vec4 frag_color;

uniform sampler2D basic_texture;

void main () {
    vec3 light_position_eye = vec3 (view_mat * vec4 (light_position_world, 1.0));
    vec3 distance_to_light_eye = light_position_eye - position_eye;
    vec3 direction_to_light_eye = normalize(distance_to_light_eye);
    float dot_prod = dot(direction_to_light_eye, normal_eye);
    dot_prod = max(dot_prod, 0.0);

    vec3 surface_to_viewer_eye = normalize(-position_eye);
    vec3 half_way_eye = normalize(surface_to_viewer_eye + direction_to_light_eye);
    float dot_prot_specular = dot(half_way_eye, normal_eye);
    float specular_factor = pow(dot_prot_specular, specular_exponent);

    vec3 ia = la * ka;
    vec3 id = ld * kd * dot_prod;
    vec3 is = ls * ks * specular_factor;

    vec4 texel = texture(basic_texture, texture_coordinates);

    frag_color = texel * vec4 (is + id + ia, 1.0);
}

#version 410

layout(location = 0) in vec3 vertex_position;
layout(location = 1) in vec3 vertex_color;
layout(location = 2) in vec3 vertex_normal;
layout(location = 3) in vec2 uv;

out vec2 texture_coordinates;
uniform mat4 model_mat;

uniform vec2 hud_scale;

void main () {
    texture_coordinates = uv;

    gl_Position = vec4 ((vertex_position.xy * hud_scale) + model_mat[3].xy, 0.0f, 1.0f);
}
